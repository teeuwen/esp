/*
 *
 * Extremely Simple Processor
 * src/esp.c - esp main runtime libary
 *
 * Copyright (C) 2017 Bastiaan Teeuwen <bastiaan@mkcl.nl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 */

#include <stdlib.h>

#include <esp.h>

void esp_regdump(struct esp *esp)
{
	if (!esp)
		return;

	printf(
			"r1: %#x (%u)\n"
			"r2: %#x (%u)\n"
			"\n"
			"ir: %#x  sp: %#x\n"
			"flags: %#x\n"
			"pc: %#x (%u)\n",
			esp->r1, esp->r1, esp->r2, esp->r2, esp->ir, esp->sp,
			esp->flags, esp->pc, esp->pc);
}

int esp_load(struct esp *esp, FILE *bin)
{
	long s;

	if (!esp)
		return 0;

	fseek(bin, 0, SEEK_END);
	s = ftell(bin);
	fseek(bin, 0, SEEK_SET);

	return fread(esp->mem, s, 1, bin);
}

int esp_run(struct esp *esp)
{
	uint16_t buf;

	if (!esp)
		return 0;

	switch (esp->mem[esp->pc]) {
	case O_NOP:
		break;
	case O_ADD:
		if (esp->mem[++esp->pc] & A_LIT) {
			buf = esp->mem[++esp->pc] << 8;
			buf = esp->mem[++esp->pc] & 0xFF;
		}

		esp->r1 = buf;

		break;
	default:
		return 0;
	}

	esp->pc++;

	return 1;
}

struct esp *esp_alloc(int mem, int vmem)
{
	struct esp *esp = NULL;

	if (!mem)
		mem = MEM_SIZE;
	if (!vmem)
		vmem = VMEM_SIZE;

	if (!(esp = calloc(1, sizeof(struct esp))))
		goto err;

#if MEM_CLEAR
	if (!(esp->mem = calloc(1, sizeof(struct esp))))
#else
	if (!(esp->mem = malloc(sizeof(struct esp))))
#endif
		goto err;
	if (!(esp->vmem = calloc(1, sizeof(struct esp))))
		goto err;

	return esp;

err:
	if (esp) {
		free(esp->mem);
		free(esp);
	}

	return NULL;
}

void esp_free(struct esp *esp)
{
	if (!esp)
		return;

	free(esp->mem);
	free(esp->vmem);
	free(esp);
}
