#
# Makefile
#

CFLAGS		:= -Wall -Wextra -fdiagnostics-color=auto -std=c89 #-g

esp-o		= src/esp.o
espa-o		= src/espa.o

esp-i		= -I src/include

all: espa simple

PHONY += clean
clean:
	find src/ -type f -name '*.o' -exec rm {} \;
	rm -f bin/simple bin/espa

%.o: %.c
	$(CC) $(CFLAGS) -c $< $(esp-i) -o $@

PHONY += espa
espa: bin/espa
bin/espa: $(espa-o)
	mkdir -p bin/
	$(CC) -o $@ $^

PHONY += simple
simple: bin/simple
bin/simple: $(esp-o) src/simple.o
	mkdir -p bin/
	$(CC) -o $@ $^

.PHONY: $(PHONY)
