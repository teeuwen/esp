/*
 *
 * Extremely Simple Processor
 * src/espa.c - esp assembler
 *
 * Copyright (C) 2017 Bastiaan Teeuwen <bastiaan@mkcl.nl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <errno.h>

#include <esp.h>

/* Line buffer size */
#define BUF_SIZE 256

static const struct {
	uint8_t opcode;
	char *mnemonic;
} instructions[] = {
	{ O_NOP, "nop" },
	{ O_ADD, "add" },
	{ O_ADC, "adc" },
	{ O_SUB, "sub" },
	{ O_SUC, "suc" },
	{ O_MUL, "mul" },
	{ O_MUI, "mui" },
	{ O_DIV, "div" },
	{ O_DII, "dii" },
	{ O_NEG, "neg" },
	{ O_AND, "and" },
	{ O_OR, "or" },
	{ O_XOR, "xor" },
	{ O_NOT, "not" },
	{ O_ROL, "rol" },
	{ O_ROR, "ror" },
	{ O_RCL, "rcl" },
	{ O_RCR, "rcr" },
	{ O_SHL, "shl" },
	{ O_SHR, "shr" },
	{ O_SIL, "sil" },
	{ O_SIR, "sir" },
	{ O_POP, "pop" },
	{ O_PUSH, "push" },
	{ O_MOV, "mov" },
	{ O_GET, "get" },
	{ O_RET, "ret" },
	{ O_REI, "rei" },
	{ O_CALL, "call" },
	{ O_JMP, "jmp" },
	{ O_SUB, "cmp" }, /* Alias for "sub" */
	{ O_JIZ, "jiz" },
	{ O_JNZ, "jnz" },
	{ O_JIS, "jis" },
	{ O_JNS, "jns" },
	{ O_JIO, "jio" },
	{ O_JNO, "jno" },
	{ O_JIC, "jic" },
	{ O_JNC, "jnc" },
	{ O_JIB, "jib" },
	{ O_JBE, "jbe" },
	{ O_JIA, "jia" },
	{ O_JAE, "jae" },
	{ O_JIL, "jil" },
	{ O_JLE, "jle" },
	{ O_JIG, "jig" },
	{ O_JGE, "jge" },
	{ O_STC, "stc" },
	{ O_STI, "sti" },
	{ O_CLC, "clc" },
	{ O_CLI, "cli" },
	{ O_HLT, "hlt" }
};

static char *arg0;

static void die(int errnum, char *desc)
{
	if (errnum < 0)
		fprintf(stderr, "%s: %s\n", arg0, desc);
	else if (desc)
		fprintf(stderr, "%s: %s: %s\n", arg0, strerror(errnum), desc);
	else
		fprintf(stderr, "%s: %s\n", arg0, strerror(errnum));

	exit(1);
}

/*
 * This function only handles the mnemonic and operand 1, writeop() parses
 * operand 2 if it exists.
 */
static uint8_t getop(char **str, int linenum, int n)
{
	char *del;
	unsigned int i;

	/* TODO Split */

	if (n == 0) {
		del = strchr(*str, ' ');

		for (i = 0; i < sizeof(instructions) / sizeof(instructions[0]);
				i++)
			if (strncmp(*str, instructions[i].mnemonic, del - *str)
					== 0)
				break;

		*str = ++del;
		return instructions[i].opcode;
	}

	del = strchr(*str, ',');

	if (strncmp(*str, "r1", del - *str) == 0) {
		*str = ++del;
		return 1;
	} else if (strncmp(*str, "r2", del - *str) == 0) {
		*str = ++del;
		return 2;
	} else {
		*del = '\0';
		fprintf(stderr, "invalid register '%s' on line %d!\n", *str,
				linenum);
		return 0xFE;
	}
}

static void writeop(FILE *bin, uint8_t opc, uint8_t op1, char *op2)
{
	uint16_t ins, val;

	ins = opc << 8;

	if (op1)
		ins |= ((op1 & 0x07) << 5);

	/* FIXME TEMP TEMP XXX XXX XXX */
	val = 4;
	ins |= A_LIT;
	/* FIXME TEMP TEMP XXX XXX XXX */

	if (!(ins & A_LIT))
		ins |= val << 2;

	fputc((ins >> 8) & 0xFF, bin);
	fputc(ins & 0xFF, bin);

	if (ins & A_LIT) {
		fputc((val >> 8) & 0xFF, bin);
		fputc(val & 0xFF, bin);
	}
}

static int parse(FILE *bin, char *line, int linenum)
{
	uint8_t opc;

	switch ((opc = getop(&line, linenum, 0))) {
	/* No operands */
	case O_NOP:
	case O_RET:
	case O_REI:
	case O_STC:
	case O_STI:
	case O_CLC:
	case O_CLI:
	case O_HLT:
		writeop(bin, O_NOP, 0, 0);
		break;
	/* One operand */
	case O_NEG:
	case O_NOT:
	case O_ROL:
	case O_ROR:
	case O_RCL:
	case O_RCR:
	case O_SHL:
	case O_SHR:
	case O_SIL:
	case O_SIR:
	case O_POP:
	case O_PUSH:
	case O_CALL:
	case O_JMP:
	case O_JIZ:
	case O_JNZ:
	case O_JIS:
	case O_JNS:
	case O_JIO:
	case O_JNO:
	case O_JIC:
	case O_JNC:
	case O_JIB:
	case O_JBE:
	case O_JIA:
	case O_JAE:
	case O_JIL:
	case O_JLE:
	case O_JIG:
	case O_JGE:
		writeop(bin, opc, 0, line);
		break;
	/* Two operands */
	case O_ADD:
	case O_ADC:
	case O_SUB:
	case O_SUC:
	case O_MUL:
	case O_MUI:
	case O_DIV:
	case O_DII:
	case O_AND:
	case O_OR:
	case O_XOR:
	case O_MOV:
	case O_GET:
		writeop(bin, opc, getop(&line, linenum, 1), line);
		break;
	/* TODO */
	case O_RES:
	default:
		return 0;
	}

	return 1;
}

int main(int argc, char **argv)
{
	FILE *src, *bin;
	char line[BUF_SIZE], *bin_name, *c;
	int linenum;

	arg0 = *argv++;

	if (argc != 2)
		die(EINVAL, NULL);

	if (!(src = fopen(*argv, "r")))
		die(errno, *argv);

	if (!(bin_name = malloc(strlen(*argv) + 1))) {
		fclose(src);
		die(errno, NULL);
	}

	strcpy(bin_name, *argv);

	c = strrchr((const char *) bin_name, '.');

	if ((unsigned long) (c - bin_name + 1) == strlen(bin_name)) {
		fclose(src);
		die(0, "who ends a filename with a dot?");
	}

	*++c = 'o';
	*++c = '\0';

	if (!(bin = fopen(bin_name, "w+")))
		die(errno, bin_name);

	for (linenum = 1; fgets(line, BUF_SIZE, src); linenum++)
		if (!parse(bin, line, linenum))
			goto err;

	fclose(src);
	fclose(bin);

	return 0;

err:
	fclose(src);
	fclose(bin);

	die(0, "error parsing file");

	/* FIXME meh */
	return 1;
}
