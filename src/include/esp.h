/*
 *
 * Extremely Simple Processor
 * src/esp.h - main header file
 *
 * Copyright (C) 2017 Bastiaan Teeuwen <bastiaan@mkcl.nl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 */

#ifndef _ESP_H
#define _ESP_H

#include <stdio.h>
#include <stdint.h>

/* Default memory size - 640K ought to be enough right? */
#define MEM_SIZE	64000
/* Default video memory size */
#define VMEM_SIZE	(80 * 25)
/* Clear memory on initialization? */
#define MEM_CLEAR	1

/* Status flags */
#define F_Z	0x00	/* Zero flag */
#define F_O	0x02	/* Overflow flag */
#define F_C	0x04	/* Carry flag */
#define F_I	0x08	/* Interrupt flag */

struct esp {
	/* Main registers */
	uint16_t r1, r2;

	/* Index registers */
	uint16_t ir, sp;

	/* Status registers */
	uint8_t flags;

	/* Program counter */
	uint16_t pc;

	/* Main and video memory */
	uint8_t *mem, *vmem;
};

/* Interrupts */
#define I_ZERO	0x00	/* Division by zero */
#define I_OVF	0x01	/* Overflow */
#define I_INV	0x02	/* Invalid opcode */
#define I_GPF	0x03	/* General protection fault */

/* Addressing mode */
#define A_LIT	0x01	/* Literal */
#define A_IND	0x02	/* Indirect */

/* Opcodes */
#define O_NOP	0x00	/* No Operation */

#define O_ADD	0x01	/* Addition */
#define O_ADC	0x02	/* Addition with carry */

#define O_SUB	0x03	/* Subtraction / compare */
#define O_SUC	0x04	/* Subtraction with carry */

#define O_MUL	0x05	/* Multiply (unsigned) */
#define O_MUI	0x06	/* Multiply (signed) */

#define O_DIV	0x07	/* Division (unsigned) */
#define O_DII	0x08	/* Division (signed) */

#define O_NEG	0x09	/* Negation */

#define O_AND	0x0A	/* Logical AND */
#define O_OR	0x0B	/* Logical OR */
#define O_XOR	0x0C	/* Exclusive OR */
#define O_NOT	0x0D	/* Logical NOT */

#define O_ROL	0x0E	/* Rotate left */
#define O_ROR	0x0F	/* Rotate right */

#define O_RCL	0x10	/* Rotate left (with carry) */
#define O_RCR	0x11	/* Rotate right (with carry) */

#define O_SHL	0x12	/* Shift left (unsigned) */
#define O_SHR	0x13	/* Shift right (unsigned) */

#define O_SIL	0x14	/* Shift left (signed) */
#define O_SIR	0x15	/* Shift right (signed) */

#define O_POP	0x16	/* Pop from stack */
#define O_PUSH	0x17	/* Push to stack */

#define O_MOV	0x18	/* Move data to memory */
#define O_GET	0x19	/* Get data from memory */

#define O_RET	0x1A	/* Return from procedure */
#define O_REI	0x1B	/* Return from interrupt */

#define O_CALL	0x1C	/* Jump to subroutine */

#define O_JMP	0x1D	/* Unconditional jump */

#define O_JIZ	0x1E	/* Jump if zero */
#define O_JNZ	0x1F	/* Jump if not zero */

#define O_JIS	0x20	/* Jump if signed */
#define O_JNS	0x21	/* Jump if not signed */

#define O_JIO	0x22	/* Jump if overflow flag is set */
#define O_JNO	0x23	/* Jump if overflow flag is not set */

#define O_JIC	0x24	/* Jump if carry flag is set */
#define O_JNC	0x25	/* Jump if carry flag is not set */

#define O_JIB	0x26	/* Jump if below (<) (unsigned) */
#define O_JBE	0x27	/* Jump if below or equal (<=) (unsigned) */

#define O_JIA	0x28	/* Jump if above (>) (unsigned) */
#define O_JAE	0x29	/* Jump if above or equal (>=) (unsigned) */

#define O_JIL	0x2A	/* Jump if below (<) (signed) */
#define O_JLE	0x2B	/* Jump if below or equal (<=) (signed) */

#define O_JIG	0x2C	/* Jump if above (>) (signed) */
#define O_JGE	0x2D	/* Jump if above or equal (>=) (signed) */

#define O_STC	0x2E	/* Set carry flag */
#define O_STI	0x2F	/* Set interrupt flag */

#define O_CLC	0x30	/* Clear carry flag */
#define O_CLI	0x31	/* Clear interrupt flag */

/* Opcodes 0x32 - 0xFD are unused */

#define O_RES	0xFE	/* Reserved */
#define O_HLT	0xFF	/* Halt the CPU */

void esp_regdump(struct esp *esp);

int esp_load(struct esp *esp, FILE *bin);
int esp_run(struct esp *esp);

struct esp *esp_alloc(int mem, int vmem);
void esp_free(struct esp *esp);

#endif
