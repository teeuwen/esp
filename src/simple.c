/*
 *
 * Extremely Simple Processor
 * src/simple.c - esp simple register dump frontend
 *
 * Copyright (C) 2017 Bastiaan Teeuwen <bastiaan@mkcl.nl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 */

#include <stdio.h>

#include <esp.h>

int main(int argc, char **argv)
{
	struct esp *esp;
	FILE *bin;

	/* TODO argument for mem size, vmemsize and such */

	if (argc != 2)
		return 1;

	if (!(bin = fopen(argv[1], "r")))
		return 1;

	if (!(esp = esp_alloc(0, 0)))
		return 1;
	if (!esp_load(esp, bin))
		return 1;
	fclose(bin);

	while (esp_run(esp));
	esp_regdump(esp);

	esp_free(esp);

	return 0;
}
